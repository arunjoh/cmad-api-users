package com.cisco.cmad.so.util;

import org.bson.BsonDocument;
import org.bson.BsonInt32;
import org.bson.BsonValue;

import com.cisco.cmad.so.service.UsersService;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.mongo.IndexOptions;
import io.vertx.ext.mongo.MongoClient;

public class MongoUtil {

    private static Logger logger = LoggerFactory.getLogger(MongoUtil.class.getName());

    public static final String ENV_MONGODB_DATABASE = "SO_MONGODB_DATABASE";

    public static final String ENV_MONGODB_URL = "SO_MONGODB_URL";

    private static final String DEFAULT_MONGODB_DATABASE = "cmad";

    private static final String DEFAULT_MONGODB_URL = "mongodb://localhost:27017";

    private static MongoClient client = null;

    private MongoUtil() {
    }

    public static synchronized void initialize(Vertx vertx) {
        if (client != null)
            return;

        logger.info("Initializing mongodb client");

        String mongoDbName = DEFAULT_MONGODB_DATABASE;
        String mongoDbUrl = DEFAULT_MONGODB_URL;

        // Over-ride defaults from Environment variable, if available.
        String envMongoDbName = System.getenv(ENV_MONGODB_DATABASE);
        if (envMongoDbName != null && !envMongoDbName.isEmpty()) {
            logger.info(
                    "Found and using environment variable " + ENV_MONGODB_DATABASE + " with value " + envMongoDbName);
            mongoDbName = envMongoDbName;
        } else {
            logger.info("Using default mongodb database : " + DEFAULT_MONGODB_DATABASE);
        }

        String envMongoDbUrl = System.getenv(ENV_MONGODB_URL);
        if (envMongoDbUrl != null && !envMongoDbUrl.isEmpty()) {
            logger.info("Found and using environment variable " + ENV_MONGODB_URL);
            mongoDbUrl = envMongoDbUrl;
        } else {
            logger.info("Using default mongodb url : " + DEFAULT_MONGODB_URL);
        }

        JsonObject config = new JsonObject();
        config.put("db_name", mongoDbName);
        config.put("connection_string", mongoDbUrl);
        client = MongoClient.createShared(vertx, config);

        // Create indexes on the MongoDB instance.
        initializeIndices();
    }

    public static MongoClient getMongoClient() {
        return client;
    }

    public static boolean isInitialized() {
        return client != null;
    }

    private static void initializeIndices() {

        // Indexes for Users collection.
        logger.info("Initializing indices for users collection");
        
        // Add index on first name and last to allow searching for users by name.
        JsonObject searchIndex = new JsonObject();
        searchIndex.put("firstName", "text").put("lastName", "text").put("userName", "text");
        
        // Give email id twice as much weight as first or last name.
        IndexOptions options = new IndexOptions();
        JsonObject weights = new JsonObject().put("firstName", 5).put("lastName", 5).put("userName", 10);
        options.weights(weights);
        
        getMongoClient().createIndexWithOptions(UsersService.USERS, searchIndex, options, result -> {
            if (result.failed()) {
                logger.error("Error created name and id search index on users.", result.cause());
            }
        });
    }
    
}
