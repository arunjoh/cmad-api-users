package com.cisco.cmad.so.service;

import com.cisco.cmad.so.util.MongoUtil;

import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTOptions;
import io.vertx.ext.mongo.FindOptions;

public class UsersService {

    private static final Logger logger = LoggerFactory.getLogger(UsersService.class.getName());

    public static final String USERS = "users";
    public static final String USER_ID = "userName";

    private JWTAuth provider;

    public void setProvider(JWTAuth provider) {
        this.provider = provider;
    }

    public void createUser(Message<Object> message) {

        JsonObject document = new JsonObject(message.body().toString());
        logger.debug("Creating a new user." + document.toString());
        String userName = document.getString("userName");

        // document.put("_id", userName);
        logger.debug("Creating a new user." + document.toString());
        // check if user already exists in DB.
        // insert DB Entry
        MongoUtil.getMongoClient().insert(USERS, document, reply -> {
            if (reply.succeeded()) {
                message.reply(reply.result());
            } else {
                logger.error("Error Creating user in Database", reply.cause());
                message.fail(500, "Database error.");
            }
        });
    }

    public void searchUsers(Message<Object> message) {
    	System.out.println("Search users from database.");
    	logger.debug("Search users from database.");
        JsonObject document = new JsonObject(message.body().toString());

        int skip = document.getInteger("skip");
        int limit = document.getInteger("limit");
        String text = document.getString("text");

        FindOptions findOptions = new FindOptions();
        // Apply skip and limit.
        findOptions.setLimit(limit);
        findOptions.setSkip(skip);

        JsonObject query = new JsonObject();
        JsonObject sort = new JsonObject();
        JsonObject fields = new JsonObject();

        // Search by question text.
        // If no text is given, get all questions
        if (text != null && text.trim().length() > 0) {
            JsonObject subQuery = new JsonObject();
            subQuery.put("$search", text);
            query.put("$text", subQuery);

            // Include search score metadata in the result
            JsonObject metaField = new JsonObject();
            metaField.put("$meta", "textScore");
            fields.put("searchScore", metaField);

            // Sort by relevance and then by date.
            sort.put("searchScore", metaField);
        }

        findOptions.setFields(fields);
        findOptions.setSort(sort);

        JsonObject result = new JsonObject();

        MongoUtil.getMongoClient().count(USERS, query, countReply -> {
            if (countReply.succeeded()) {
                result.put("total", countReply.result());
                MongoUtil.getMongoClient().findWithOptions(USERS, query, findOptions, searchReply -> {
                    if (searchReply.succeeded()) {
                        JsonArray resultArray = new JsonArray(searchReply.result());
                        result.put("data", resultArray);
                        message.reply(result.toString());
                    } else {
                        logger.error("Error searching users from database", searchReply.cause());
                        message.fail(500, "Database error.");
                    }
                });
            } else {
                logger.error("Error counting search users from database", countReply.cause());
                message.fail(500, "Database error.");
            }
        });
    }

    public void getUserById(Message<Object> message) {

    }

    public void updateUser(Message<Object> message) {
    	logger.debug("Updating user...");

		JsonObject user = new JsonObject(message.body().toString());
		String questionId = user.getString(USER_ID);

		JsonObject update = new JsonObject().put("$set", new JsonObject());
		for (String field : user.fieldNames()) {
			update.getJsonObject("$set").put(field, user.getValue(field));
		}

		JsonObject query = new JsonObject().put(USER_ID, questionId);
		
		MongoUtil.getMongoClient().updateCollection(USERS, query, update, reply -> {
			if (reply.succeeded()) {
				logger.debug("Matched : " + reply.result().getDocMatched());
				logger.debug("Updated : " + reply.result().getDocModified());
				if (reply.result().getDocMatched() == 0) {
					message.fail(404, "User does not exist.");
				} else {
					message.reply(reply.result().toString());
				}
			} else {
				logger.error("Error in database while updating user...", reply.cause());
				message.fail(500, "Database error.");
			}
		});
    }

    public void authenticate(Message<Object> message) {

        logger.info("authenticate the user.");
        JsonObject document = new JsonObject(message.body().toString());
        String userName = document.getString("userName");
        String password = document.getString("password");
        JsonObject query = new JsonObject();
        query.put("userName", userName);

        logger.info("find the query." + query);

        MongoUtil.getMongoClient().findOne(USERS, query, null, reply -> {
            if (reply.succeeded()) {

                JsonObject dbRecord = reply.result();
                String dbPassword = dbRecord.getString("password");
                String token = "";

                if (dbPassword.equals(password)) {
                    token = generateUserToken(dbRecord);
                    dbRecord.put("jwtToken", token);
                }
                message.reply(dbRecord.toString());

            } else {
                logger.error("Error getting question by ID from database", reply.cause());
                message.fail(500, "Database error.");
            }
        });

    }

    private String generateUserToken(JsonObject dbRecord) {

        String userId = dbRecord.getString("userName");
        String firstName = dbRecord.getString("firstName");
        String lastName = dbRecord.getString("lastName");

        JsonObject userToken = new JsonObject().put("sub", userId).put("firstName", firstName);
        if (lastName != null)
            userToken.put("lastName", lastName);

        String token = provider.generateToken(userToken, new JWTOptions());
        logger.info("Token Generated:" + token);
        return token;
    }
}
